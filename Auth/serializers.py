from rest_framework.serializers import ModelSerializer

from Auth.models import MyUser


class MyUserSerializer(ModelSerializer):
    class Meta:
        model = MyUser
        exclude=('password',)
