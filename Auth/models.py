from django.db import models

from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)


class MyUserManager(BaseUserManager):
    def create_user(self, student_id, first_name,rfid_card, last_name, photo, date_of_birth, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not student_id:
            raise ValueError('Users must have an id number')

        user = self.model(
            student_id=student_id,
            first_name=first_name,
            last_name=last_name,
            photo=photo,
            rfid_card=rfid_card,
            date_of_birth=date_of_birth,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, student_id, password,rfid_card=None, first_name=None, last_name=None, photo=None, date_of_birth=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            student_id,
            password=password,
            first_name=first_name,
            last_name=last_name,
            photo=photo,
            date_of_birth=date_of_birth,
            rfid_card=rfid_card
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser):
    student_id = models.CharField(max_length=240, unique=True)
    first_name = models.CharField(max_length=240, null=True)
    last_name = models.CharField(max_length=240, null=True)
    photo = models.ImageField(null=True, blank=True)
    date_of_birth = models.DateField(null=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    rfid_card = models.CharField(max_length=240,null=True)
    objects = MyUserManager()

    USERNAME_FIELD = 'student_id'

    def __str__(self):
        return self.student_id

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

