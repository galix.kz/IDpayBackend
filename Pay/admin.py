from django.contrib import admin
from Pay.models import PayHistory,StudentPay,StudentPayHistory
# Register your models here.
admin.site.register(PayHistory)
admin.site.register(StudentPay)
admin.site.register(StudentPayHistory)