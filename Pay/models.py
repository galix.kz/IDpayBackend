from django.db import models
from django.db.models import CASCADE


class StudentPayManager(models.Manager):
    def minus_balance(self, rfid, amount_minus, *args, **kwargs):
        student_pay = super().get_queryset().all().filter(student__rfid_card=rfid).first()
        student_pay.balance = student_pay.balance - amount_minus
        student_pay.save()
        return student_pay


class StudentPay(models.Model):
    student = models.ForeignKey('Auth.MyUser', on_delete=CASCADE)
    balance = models.IntegerField()
    objects = StudentPayManager()

    def __str__(self):
        return str(self.student.student_id) + ' - ' + str(self.balance)


class PayHistory(models.Model):
    place = models.ForeignKey('Auth.MyUser', on_delete=CASCADE)
    sum = models.IntegerField()
    date = models.DateField(auto_now=True)

    def __str__(self):
        return str(self.place) + ' - ' + str(self.sum) + ' - ' + str(self.date)


class StudentPayHistory(models.Model):
    student_pay = models.ForeignKey('Pay.StudentPay', on_delete=CASCADE)
    pay_history = models.ForeignKey('Pay.PayHistory', on_delete=CASCADE)

    def __str__(self):
        return str(self.student_pay.student.student_id) + " " + str(self.pay_history.__str__())
