from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import list_route
from rest_framework.permissions import SAFE_METHODS, AllowAny
from rest_framework.response import Response

from Auth.models import MyUser
from Pay.models import PayHistory, StudentPay, StudentPayHistory
from rest_framework.viewsets import ModelViewSet,GenericViewSet
from Pay.serializers import PayReadSerializer, PayWriteSerializer, StudentPayHistoryReadSerializer, \
    PayHistoryWriteSerializer, \
    StudentPayHistoryWriteSerializer, PayHistoryReadSerializer
from rest_framework.mixins import RetrieveModelMixin,ListModelMixin


class StudentPayViewSet(RetrieveModelMixin,ListModelMixin,GenericViewSet):
    queryset = StudentPay.objects.all()

    def get_serializer_class(self, *args, **kwargs):
        # print(self.request.method)

        if self.request is not None and self.request.method in SAFE_METHODS :
            return PayReadSerializer
        return PayWriteSerializer


    @list_route(methods=['POST'], permission_classes=[AllowAny])
    def minus(self, request, *args, **kwargs):
        # amount = request.data['amount']
        rfid = request.data['rfid']
        place_rfid = request.data['place_rfid']
        student_pay = StudentPay.objects.minus_balance(rfid,500)
        place = MyUser.objects.all().filter(rfid_card=place_rfid).first()
        pay_history=PayHistory.objects.create(place=place,sum=500)
        student_pay_history = StudentPayHistory.objects.create(student_pay=student_pay,pay_history=pay_history)
        return Response({"status": "ok"}, status=status.HTTP_200_OK)


class PayHistoryViewSet(RetrieveModelMixin,ListModelMixin,GenericViewSet):
    queryset = PayHistory.objects.all()

    def get_serializer_class(self, *args, **kwargs):
        if self.request is not None and self.request.method in SAFE_METHODS:
            return PayHistoryReadSerializer
        return PayHistoryWriteSerializer


class StudentPayHistoryViewSet(RetrieveModelMixin,ListModelMixin,GenericViewSet):
    queryset = StudentPay.objects.all()

    def get_serializer_class(self, *args, **kwargs):
        if self.request is not None and self.request.method in SAFE_METHODS:
            return StudentPayHistoryReadSerializer
        return StudentPayHistoryWriteSerializer
