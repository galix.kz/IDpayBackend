from rest_framework.serializers import ModelSerializer

from Auth.serializers import MyUserSerializer
from Pay.models import StudentPay, PayHistory, StudentPayHistory


class PayReadSerializer(ModelSerializer):
    student = MyUserSerializer()

    class Meta:
        model = StudentPay
        fields = '__all__'


class PayWriteSerializer(ModelSerializer):
    class Meta:
        model = StudentPay
        fields = '__all__'


class PayHistoryReadSerializer(ModelSerializer):
    place = MyUserSerializer()

    class Meta:
        model = PayHistory
        fields = '__all__'


class PayHistoryWriteSerializer(ModelSerializer):
    class Meta:
        model = PayHistory
        fields = '__all__'


class StudentPayHistoryReadSerializer(ModelSerializer):
    student_pay = PayReadSerializer()
    pay_history = PayHistoryReadSerializer()

    class Meta:
        model = StudentPayHistory
        fields = '__all__'


class StudentPayHistoryWriteSerializer(ModelSerializer):
    class Meta:
        model = StudentPayHistory
        fields = '__all__'
